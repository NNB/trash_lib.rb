# frozen_string_literal: true

require_relative 'lib/trash/version'

Gem::Specification.new do |spec|
  spec.name = 'trash_lib'
  spec.version = Trash::VERSION
  spec.authors = ['NNB']
  spec.email = ['nnbnh@protonmail.com']

  spec.summary = "A library for system file's trash."
  spec.description = "This is a library for system file's trash in FreeDesktop Trash compliant environments."
  spec.homepage = 'https://codeberg.org/NNB/trash.rb'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/src/branch/main/CHANGELOG.md"

  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.extra_rdoc_files = ['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.require_paths = ['lib']
end
