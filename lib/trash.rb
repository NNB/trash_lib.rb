# frozen_string_literal: true

require 'fileutils'
require 'time'

require_relative 'trash/version'

# Namespace for system file's trash.
module Trash
  TRASH_PATH = File.join Dir.home, %w[.local share Trash]
  TRASH_FILE_PATH = File.join TRASH_PATH, 'files'
  TRASH_INFO_PATH = File.join TRASH_PATH, 'info'

  # A Entry object is a representation of a trash file.
  class Entry < File
    # Returns the original path for file.
    def origin
      info[/^Path=(.*)$/, 1]
    end

    # Returns the deletion time for file.
    def dtime
      info[/^DeletionDate=(.*)$/, 1]
    end

    # Restore file to original path.
    def restore
      FileUtils.move(trash_path, origin, secure: true)
      delete_info

      nil
    end

    # Permanently delete the file.
    def delete
      FileUtils.rm_rf(trash_path, secure: true)
      delete_info

      nil
    end

    private

    def trash_path
      File.join(TRASH_FILE_PATH, path)
    end

    def info
      File.file?(info_path) ? File.read(info_path) : ''
    end

    def info_path
      File.join(TRASH_INFO_PATH, "#{File.basename(path)}.trashinfo")
    end

    def delete_info
      FileUtils.rm_rf(info_path, secure: true)
    end
  end

  class << self
    # List trashed files.
    def list
      make_trash_paths

      Dir.chdir TRASH_FILE_PATH do
        Dir.children('.').map { Entry.new _1 }.select { _1.origin && _1.dtime }.sort_by(&:dtime)
      end
    end

    # Put files in trash.
    def put(*paths)
      make_trash_paths

      paths.flatten.map do |path|
        trash_name = trash_name(path)
        trash_path = File.join(TRASH_FILE_PATH, trash_name)

        FileUtils.move(path, trash_path, secure: true)
        make_trash_info(path, trash_name)

        Dir.chdir TRASH_FILE_PATH do
          Entry.new(File.basename(trash_path))
        end
      end
    end

    # Empty the trash.
    def empty
      FileUtils.rm_rf([TRASH_FILE_PATH, TRASH_INFO_PATH], secure: true)
      make_trash_paths

      nil
    end

    private

    def make_trash_paths
      [TRASH_FILE_PATH, TRASH_INFO_PATH].each { FileUtils.mkpath _1 }
    end

    def trash_name(path)
      trash_name = File.basename(path)

      match_index = 1
      while File.exist? File.join(TRASH_FILE_PATH, trash_name)
        match_index += 1
        trash_name = File.basename(path).split('.').insert(1, match_index).compact.join('.')
      end

      trash_name
    end

    def make_trash_info(path, trash_name)
      File.write(
        File.join(TRASH_INFO_PATH, "#{trash_name}.trashinfo"),
        <<~TRASH_INFO
          [Trash Info]
          Path=#{File.absolute_path path}
          DeletionDate=#{Time.now.strftime '%Y-%m-%dT%T'}
        TRASH_INFO
      )
    end
  end

  private_constant :Entry, :TRASH_PATH, :TRASH_FILE_PATH, :TRASH_INFO_PATH
end
