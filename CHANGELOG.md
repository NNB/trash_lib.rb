## [1.0.2] - 2023-08-16

- Hot fixed `Trash::Entry#restore` and `Trash::Entry#delete`.

## [1.0.1] - 2023-08-16

- Hot fixed `Trash::put`.

## [1.0.0] - 2023-08-16

- Initial release.
