<img width="100%" src="https://capsule-render.vercel.app/api?type=waving&section=header&color=DC2626&fontColor=FEF2F2&height=256&text=Trash%20Lib&desc=A%20library%20for%20system%20file%27s%20trash&fontAlignY=40" />

## 💡 About

This is a library for system file's trash in FreeDesktop Trash compliant environments.

## 📥 Installation

Install the gem and add to the application's Gemfile by executing:

```sh
bundle add trash_lib
```

If bundler is not being used to manage dependencies, install the gem by executing:

```sh
gem install trash_lib
```

## ⌨️ Usage

In Ruby do:

```rb
require 'trash'
```

Put files in trash:

```rb
Trash.put 'path/to/file'
Trash.put 'file1', 'file2', 'file3'
```

Empty the trash:

```rb
Trash.empty
```

List trashed files:

```rb
Trash.list
#=> [#<Trash::Entry:file1>,
     #<Trash::Entry:file2>,
     #<Trash::Entry:file3>]

trash_file = Trash.list.last
```

Returns the original path for file:

```rb
trash_file.origin  #=> 'original/path/to/file'
```

Returns the deletion time for file:

```rb
trash_file.dtime  #=> 2023-08-15 19:15:32.119439287 +0700
```

Restore file to original path:

```rb
trash_file.restore
```

Permanently delete the file:

```rb
trash_file.delete
```

## 💌 Credits

- [**Trashy**](https://gitlab.com/trashy/trashy) by [Klaatu](https://gitlab.com/trashy/trashy#contact)
- [**Trash-CLI**](https://github.com/andreafrancia/trash-cli) by [Andrea Francia](https://github.com/andreafrancia)

<a href="https://codeberg.org/NNB"><img width="100%" src="https://capsule-render.vercel.app/api?type=waving&section=footer&color=DC2626&fontColor=FEF2F2&height=128&desc=Made%20with%20%26lt;3%20by%20NNB&descAlignY=80" /></a>
